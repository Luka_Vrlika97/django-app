# Generated by Django 3.1.5 on 2021-01-07 21:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='summary',
            field=models.TextField(default='This is a summary!'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.TextField(),
        ),
    ]
